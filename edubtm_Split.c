/******************************************************************************/
/*                                                                            */
/*    ODYSSEUS/EduCOSMOS Educational-Purpose Object Storage System            */
/*                                                                            */
/*    Developed by Professor Kyu-Young Whang et al.                           */
/*                                                                            */
/*    Database and Multimedia Laboratory                                      */
/*                                                                            */
/*    Computer Science Department and                                         */
/*    Advanced Information Technology Research Center (AITrc)                 */
/*    Korea Advanced Institute of Science and Technology (KAIST)              */
/*                                                                            */
/*    e-mail: kywhang@cs.kaist.ac.kr                                          */
/*    phone: +82-42-350-7722                                                  */
/*    fax: +82-42-350-8380                                                    */
/*                                                                            */
/*    Copyright (c) 1995-2013 by Kyu-Young Whang                              */
/*                                                                            */
/*    All rights reserved. No part of this software may be reproduced,        */
/*    stored in a retrieval system, or transmitted, in any form or by any     */
/*    means, electronic, mechanical, photocopying, recording, or otherwise,   */
/*    without prior written permission of the copyright owner.                */
/*                                                                            */
/******************************************************************************/
/*
 * Module: edubtm_Split.c
 *
 * Description : 
 *  This file has three functions about 'split'.
 *  'edubtm_SplitInternal(...) and edubtm_SplitLeaf(...) insert the given item
 *  after spliting, and return 'ritem' which should be inserted into the
 *  parent page.
 *
 * Exports:
 *  Four edubtm_SplitInternal(ObjectID*, BtreeInternal*, Two, InternalItem*, InternalItem*)
 *  Four edubtm_SplitLeaf(ObjectID*, PageID*, BtreeLeaf*, Two, LeafItem*, InternalItem*)
 */


#include <string.h>
#include "EduBtM_common.h"
#include "BfM.h"
#include "EduBtM_Internal.h"



/*@================================
 * edubtm_SplitInternal()
 *================================*/
/*
 * Function: Four edubtm_SplitInternal(ObjectID*, BtreeInternal*,Two, InternalItem*, InternalItem*)
 *
 * Description:
 * (Following description is for original ODYSSEUS/COSMOS BtM.
 *  For ODYSSEUS/EduCOSMOS EduBtM, refer to the EduBtM project manual.)
 *
 *  At first, the function edubtm_SplitInternal(...) allocates a new internal page
 *  and initialize it.  Secondly, all items in the given page and the given
 *  'item' are divided by halves and stored to the two pages.  By spliting,
 *  the new internal item should be inserted into their parent and the item will
 *  be returned by 'ritem'.
 *
 *  A temporary page is used because it is difficult to use the given page
 *  directly and the temporary page will be copied to the given page later.
 *
 * Returns:
 *  error code
 *    some errors caused by function calls
 *
 * Note:
 *  The caller should call BfM_SetDirty() for 'fpage'.
 */
Four edubtm_SplitInternal(
    ObjectID                    *catObjForFile,         /* IN catalog object of B+ tree file */
    BtreeInternal               *fpage,                 /* INOUT the page which will be splitted */
    Two                         high,                   /* IN slot No. for the given 'item' */
    InternalItem                *item,                  /* IN the item which will be inserted */
    InternalItem                *ritem)                 /* OUT the item which will be returned by spliting */
{
    Four                        e;                      /* error number */
    Two                         i;                      /* slot No. in the given page, fpage */
    Two                         j;                      /* slot No. in the splitted pages */
    Two                         k;                      /* slot No. in the new page */
    Two                         maxLoop;                /* # of max loops; # of slots in fpage + 1 */
    Four                        sum;                    /* the size of a filled area */
    Boolean                     flag=FALSE;             /* TRUE if 'item' become a member of fpage */
    PageID                      newPid;                 /* for a New Allocated Page */
    BtreeInternal               *npage;                 /* a page pointer for the new allocated page */
    Two                         fEntryOffset;           /* starting offset of an entry in fpage */
    Two                         nEntryOffset;           /* starting offset of an entry in npage */
    Two                         entryLen;               /* length of an entry */
    btm_InternalEntry           *fEntry;                /* internal entry in the given page, fpage */
    btm_InternalEntry           *nEntry;                /* internal entry in the new page, npage*/
    Boolean                     isTmp;

    /*
     * JBS Code
     */
    
    BtreeInternal tpage;
    Two alignedKlen;
    
    btm_AllocPage(catObjForFile, &(fpage->hdr.pid), &newPid);
    edubtm_InitInternal(&newPid, FALSE, FALSE);
    BfM_GetNewTrain(&newPid, (char **)&npage, PAGE_BUF);
    
    maxLoop = fpage->hdr.nSlots+1;
    sum = 0;

    tpage.hdr.pid = fpage->hdr.pid;
    tpage.hdr.flags = BTREE_PAGE_TYPE;
    tpage.hdr.type = fpage->hdr.type;
    tpage.hdr.p0 = fpage->hdr.p0;
    tpage.hdr.nSlots = 0;
    tpage.hdr.free = 0;
    tpage.hdr.unused = 0;
    
    for (i=0; i<maxLoop; ++i) {
        if (sum < BL_HALF) {
            Two l_idx = -i;
            
            fEntryOffset = fpage->slot[l_idx];
            if (i == high) { // Insert Selected Item
                nEntry = (btm_InternalEntry*)&tpage.data[sum];
                nEntry->spid = item->spid;
                nEntry->klen = item->klen;
                memcpy(nEntry->kval, item->kval, item->klen);
         
                tpage.slot[-tpage.hdr.nSlots] = sum;
                sum+=sizeof(Two)+sizeof(Four)+ALIGNED_LENGTH(item->klen);
                tpage.hdr.nSlots++;
            } else if (fEntryOffset != NIL && i!=fpage->hdr.nSlots) {
                fEntry = (btm_InternalEntry*)&fpage->data[fEntryOffset];
                
                alignedKlen = ALIGNED_LENGTH(fEntry->klen);
                entryLen = sizeof(Two)+sizeof(Four) + alignedKlen;
                
                memcpy(tpage.data+sum, fEntry, entryLen);
                tpage.slot[-tpage.hdr.nSlots] = sum;
                
                sum += entryLen;
                tpage.hdr.nSlots++;
            }
        } else {
            break;
        }
    }
    
    tpage.hdr.free = sum;
    sum = 0;

    for (;i<maxLoop; ++i) {
        Two l_idx = -i;
        fEntryOffset = fpage->slot[l_idx];
        
        if (i == high) { // Insert Selected Item
            nEntry = (btm_InternalEntry*)&npage->data[sum];
            nEntry->spid = item->spid;
            nEntry->klen = item->klen;
            memcpy(nEntry->kval, item->kval, item->klen);
         
            npage->slot[-npage->hdr.nSlots] = sum;
            sum+=sizeof(Two)+sizeof(Four)+ALIGNED_LENGTH(item->klen);
            npage->hdr.nSlots++;
        } else if (fEntryOffset != NIL && i!=fpage->hdr.nSlots) {
            fEntry = (btm_InternalEntry*)&fpage->data[fEntryOffset];
            alignedKlen = ALIGNED_LENGTH(fEntry->klen);
            entryLen = sizeof(Two)+sizeof(Four) + alignedKlen;
            
            memcpy(npage->data+sum, fEntry, entryLen);
            
            npage->slot[-npage->hdr.nSlots] = sum;
            sum += entryLen;
            npage->hdr.nSlots++;
        }
    }
    
    npage->hdr.free = sum;

    nEntryOffset = npage->slot[0];
    nEntry = (btm_InternalEntry*)&npage->data[nEntryOffset];
    npage->hdr.p0 = nEntry->spid;
    
    ritem->spid = newPid.pageNo;
    ritem->klen = nEntry->klen;
    memcpy(ritem->kval, nEntry->kval, nEntry->klen);
    
    
    memcpy(fpage, &tpage, sizeof(BtreeInternal));
    
    BfM_SetDirty(&newPid, PAGE_BUF);
    BfM_FreeTrain(&newPid, PAGE_BUF);
    
    return(eNOERROR);
    
} /* edubtm_SplitInternal() */



/*@================================
 * edubtm_SplitLeaf()
 *================================*/
/*
 * Function: Four edubtm_SplitLeaf(ObjectID*, PageID*, BtreeLeaf*, Two, LeafItem*, InternalItem*)
 *
 * Description: 
 * (Following description is for original ODYSSEUS/COSMOS BtM.
 *  For ODYSSEUS/EduCOSMOS EduBtM, refer to the EduBtM project manual.)
 *
 *  The function edubtm_SplitLeaf(...) is similar to edubtm_SplitInternal(...) except
 *  that the entry of a leaf differs from the entry of an internal and the first
 *  key value of a new page is used to make an internal item of their parent.
 *  Internal pages do not maintain the linked list, but leaves do it, so links
 *  are properly updated.
 *
 * Returns:
 *  Error code
 *  eDUPLICATEDOBJECTID_BTM
 *    some errors caused by function calls
 *
 * Note:
 *  The caller should call BfM_SetDirty() for 'fpage'.
 */
Four edubtm_SplitLeaf(
    ObjectID                    *catObjForFile, /* IN catalog object of B+ tree file */
    PageID                      *root,          /* IN PageID for the given page, 'fpage' */
    BtreeLeaf                   *fpage,         /* INOUT the page which will be splitted */
    Two                         high,           /* IN slotNo for the given 'item' */
    LeafItem                    *item,          /* IN the item which will be inserted */
    InternalItem                *ritem)         /* OUT the item which will be returned by spliting */
{
    Four                        e;              /* error number */
    Two                         i;              /* slot No. in the given page, fpage */
    Two                         j;              /* slot No. in the splitted pages */
    Two                         k;              /* slot No. in the new page */
    Two                         maxLoop;        /* # of max loops; # of slots in fpage + 1 */
    Four                        sum;            /* the size of a filled area */
    PageID                      newPid;         /* for a New Allocated Page */
    PageID                      nextPid;        /* for maintaining doubly linked list */
    BtreeLeaf                   tpage;          /* a temporary page for the given page */
    BtreeLeaf                   *npage;         /* a page pointer for the new page */
    BtreeLeaf                   *mpage;         /* for doubly linked list */
    btm_LeafEntry               *itemEntry;     /* entry for the given 'item' */
    btm_LeafEntry               *fEntry;        /* an entry in the given page, 'fpage' */
    btm_LeafEntry               *nEntry;        /* an entry in the new page, 'npage' */
    ObjectID                    *iOidArray;     /* ObjectID array of 'itemEntry' */
    ObjectID                    *fOidArray;     /* ObjectID array of 'fEntry' */
    Two                         fEntryOffset;   /* starting offset of 'fEntry' */
    Two                         nEntryOffset;   /* starting offset of 'nEntry' */
    Two                         oidArrayNo;     /* element No in an ObjectID array */
    Two                         alignedKlen;    /* aligned length of the key length */
    Two                         itemEntryLen;   /* length of entry for item */
    Two                         entryLen;       /* entry length */
    Boolean                     flag;
    Boolean                     isTmp;
 
    /*
     * JBS Code
     */
	btm_AllocPage(catObjForFile, root, &newPid);
    edubtm_InitLeaf(&newPid, FALSE, FALSE);
	BfM_GetNewTrain(&newPid, (char **)&npage, PAGE_BUF);
	
	maxLoop = fpage->hdr.nSlots+1;
    sum = 0;
    tpage.hdr.pid = fpage->hdr.pid;
    tpage.hdr.flags = BTREE_PAGE_TYPE;
    tpage.hdr.type = fpage->hdr.type;
    tpage.hdr.nSlots = 0;
    tpage.hdr.free = 0;
    tpage.hdr.prevPage = fpage->hdr.prevPage;
    tpage.hdr.nextPage = fpage->hdr.nextPage;
    tpage.hdr.unused = 0;
    for (i=0; i<maxLoop; ++i) {
        if (sum < BL_HALF) {
            Two l_idx = -i;
			
            fEntryOffset = fpage->slot[l_idx];
            if (i == high) { // Insert Selected Item
				nEntry = (btm_LeafEntry*)&tpage.data[sum];
				nEntry->nObjects = item->nObjects;
				nEntry->klen = item->klen;
				memcpy(nEntry->kval, item->kval, item->klen);
				memcpy(nEntry->kval+ALIGNED_LENGTH(item->klen), &item->oid, sizeof(ObjectID));             
                
				tpage.slot[-tpage.hdr.nSlots] = sum;
                sum+=sizeof(Two)*2+ALIGNED_LENGTH(item->klen)+sizeof(ObjectID);

                tpage.hdr.nSlots++;
            } else if (fEntryOffset != NIL && i!=fpage->hdr.nSlots) {
                fEntry = (btm_LeafEntry*)&fpage->data[fEntryOffset];
                alignedKlen = ALIGNED_LENGTH(fEntry->klen);
                entryLen = sizeof(Two)*2 + alignedKlen + sizeof(ObjectID);
                
                memcpy(tpage.data+sum, fEntry, entryLen);
                tpage.slot[-tpage.hdr.nSlots] = sum;
                
                sum += entryLen;
                tpage.hdr.nSlots++;
			}
		} else {
           break;
        }
   }
    
	tpage.hdr.free = sum;
    sum = 0;
    for (;i<maxLoop; ++i) {
        Two l_idx = -i;
        fEntryOffset = fpage->slot[l_idx];
        
        if (i == high) { // Insert Selected Item
			nEntry = (btm_LeafEntry*)&npage->data[sum];
			nEntry->nObjects = item->nObjects;
			nEntry->klen = item->klen;
			memcpy(nEntry->kval, item->kval, item->klen);
			memcpy(nEntry->kval+ALIGNED_LENGTH(item->klen), &item->oid, sizeof(ObjectID));
       
			npage->slot[-npage->hdr.nSlots] = sum;
       		sum+=sizeof(Two)*2+sizeof(ObjectID)+ALIGNED_LENGTH(item->klen);
           	npage->hdr.nSlots++;
        } else if (fEntryOffset != NIL && i!=fpage->hdr.nSlots) {
            fEntry = (btm_LeafEntry*)&fpage->data[fEntryOffset];
            alignedKlen = ALIGNED_LENGTH(fEntry->klen);
            entryLen = sizeof(Two)*2 + alignedKlen + sizeof(ObjectID);
            memcpy(npage->data+sum, fEntry, entryLen);
            npage->slot[-npage->hdr.nSlots] = sum;
            sum += entryLen;
            npage->hdr.nSlots++;
      }
    }
    npage->hdr.free = sum;
    
    memcpy(fpage, &tpage, sizeof(BtreeLeaf));
    
    // Set relationship btw npage & tpage
    nextPid.pageNo = fpage->hdr.nextPage;
    nextPid.volNo = fpage->hdr.pid.volNo;
   	
	if (nextPid.pageNo != NIL) {
		BfM_GetTrain(&nextPid, (char **)&mpage, PAGE_BUF);
	    mpage->hdr.prevPage = npage->hdr.pid.pageNo;
   		npage->hdr.nextPage = mpage->hdr.pid.pageNo;
	}
//  	printf("nSlots : %d\n", npage->hdr.nSlots);	
	fpage->hdr.nextPage = npage->hdr.pid.pageNo;
    npage->hdr.prevPage = fpage->hdr.pid.pageNo;
    
	// Produce Internal Index Entry
    nEntryOffset = npage->slot[0];
    nEntry = (btm_LeafEntry*)&npage->data[nEntryOffset];
    
    ritem->klen = nEntry->klen;
    ritem->spid = npage->hdr.pid.pageNo;
    memcpy(ritem->kval, nEntry->kval, nEntry->klen);

	if (nextPid.pageNo != NIL) {
		BfM_SetDirty(&nextPid, PAGE_BUF);
    	BfM_FreeTrain(&nextPid, PAGE_BUF);
	}
    BfM_SetDirty(&newPid, PAGE_BUF);
    BfM_FreeTrain(&newPid, PAGE_BUF);

	return(eNOERROR);
    
} /* edubtm_SplitLeaf() */
