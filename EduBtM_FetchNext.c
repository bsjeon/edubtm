/******************************************************************************/
/*                                                                            */
/*    ODYSSEUS/EduCOSMOS Educational-Purpose Object Storage System            */
/*                                                                            */
/*    Developed by Professor Kyu-Young Whang et al.                           */
/*                                                                            */
/*    Database and Multimedia Laboratory                                      */
/*                                                                            */
/*    Computer Science Department and                                         */
/*    Advanced Information Technology Research Center (AITrc)                 */
/*    Korea Advanced Institute of Science and Technology (KAIST)              */
/*                                                                            */
/*    e-mail: kywhang@cs.kaist.ac.kr                                          */
/*    phone: +82-42-350-7722                                                  */
/*    fax: +82-42-350-8380                                                    */
/*                                                                            */
/*    Copyright (c) 1995-2013 by Kyu-Young Whang                              */
/*                                                                            */
/*    All rights reserved. No part of this software may be reproduced,        */
/*    stored in a retrieval system, or transmitted, in any form or by any     */
/*    means, electronic, mechanical, photocopying, recording, or otherwise,   */
/*    without prior written permission of the copyright owner.                */
/*                                                                            */
/******************************************************************************/
/*
 * Module: EduBtM_FetchNext.c
 *
 * Description:
 *  Find the next ObjectID satisfying the given condition. The current ObjectID
 *  is specified by the 'current'.
 *
 * Exports:
 *  Four EduBtM_FetchNext(PageID*, KeyDesc*, KeyValue*, Four, BtreeCursor*, BtreeCursor*)
 */


#include <string.h>
#include "EduBtM_common.h"
#include "BfM.h"
#include "EduBtM_Internal.h"


/*@ Internal Function Prototypes */
Four edubtm_FetchNext(KeyDesc*, KeyValue*, Four, BtreeCursor*, BtreeCursor*);



/*@================================
 * EduBtM_FetchNext()
 *================================*/
/*
 * Function: Four EduBtM_FetchNext(PageID*, KeyDesc*, KeyValue*,
 *                              Four, BtreeCursor*, BtreeCursor*)
 *
 * Description:
 * (Following description is for original ODYSSEUS/COSMOS BtM.
 *  For ODYSSEUS/EduCOSMOS EduBtM, refer to the EduBtM project manual.)
 *
 *  Fetch the next ObjectID satisfying the given condition.
 * By the B+ tree structure modification resulted from the splitting or merging
 * the current cursor may point to the invalid position. So we should adjust
 * the B+ tree cursor before using the cursor.
 *
 * Returns:
 *  error code
 *    eBADPARAMETER_BTM
 *    eBADCURSOR
 *    some errors caused by function calls
 */
Four EduBtM_FetchNext(
    PageID                      *root,          /* IN root page's PageID */
    KeyDesc                     *kdesc,         /* IN key descriptor */
    KeyValue                    *kval,          /* IN key value of stop condition */
    Four                        compOp,         /* IN comparison operator of stop condition */
    BtreeCursor                 *current,       /* IN current B+ tree cursor */
    BtreeCursor                 *next)          /* OUT next B+ tree cursor */
{
    int							i;
    Four                        e;              /* error number */
    Four                        cmp;            /* comparison result */
    Two                         slotNo;         /* slot no. of a leaf page */
    Two                         oidArrayElemNo; /* element no. of the array of ObjectIDs */
    Two                         alignedKlen;    /* aligned length of key length */
    PageID                      overflow;       /* temporary PageID of an overflow page */
    Boolean                     found;          /* search result */
    ObjectID                    *oidArray;      /* array of ObjectIDs */
    BtreeLeaf                   *apage;         /* pointer to a buffer holding a leaf page */
    BtreeOverflow               *opage;         /* pointer to a buffer holding an overflow page */
    btm_LeafEntry               *entry;         /* pointer to a leaf entry */
    BtreeCursor                 tCursor;        /* a temporary Btree cursor */
  
    
    /*@ check parameter */
    if (root == NULL || kdesc == NULL || kval == NULL || current == NULL || next == NULL)
	ERR(eBADPARAMETER_BTM);
    
    /* Is the current cursor valid? */
    if (current->flag != CURSOR_ON && current->flag != CURSOR_EOS)
		ERR(eBADCURSOR);
    
    if (current->flag == CURSOR_EOS) return(eNOERROR);
    
    /* Error check whether using not supported functionality by EduBtM */
    for(i=0; i<kdesc->nparts; i++)
    {
        if(kdesc->kpart[i].type!=SM_INT && kdesc->kpart[i].type!=SM_VARSTRING)
            ERR(eNOTSUPPORTED_EDUBTM);
    }

    /*
     * JBS Code
     */
    
    edubtm_FetchNext(kdesc, kval, compOp, current, next);
    
    return(eNOERROR);
    
} /* EduBtM_FetchNext() */



/*@================================
 * edubtm_FetchNext()
 *================================*/
/*
 * Function: Four edubtm_FetchNext(KeyDesc*, KeyValue*, Four,
 *                              BtreeCursor*, BtreeCursor*)
 *
 * Description:
 * (Following description is for original ODYSSEUS/COSMOS BtM.
 *  For ODYSSEUS/EduCOSMOS EduBtM, refer to the EduBtM project manual.)
 *
 *  Get the next item. We assume that the current cursor is valid; that is.
 *  'current' rightly points to an existing ObjectID.
 *
 * Returns:
 *  Error code
 *    eBADCOMPOP_BTM
 *    some errors caused by function calls
 */
Four edubtm_FetchNext(
    KeyDesc  		*kdesc,		/* IN key descriptor */
    KeyValue 		*kval,		/* IN key value of stop condition */
    Four     		compOp,		/* IN comparison operator of stop condition */
    BtreeCursor 	*current,	/* IN current cursor */
    BtreeCursor 	*next)		/* OUT next cursor */
{
    Four 		e;		/* error number */
    Four 		cmp;		/* comparison result */
    Two 		alignedKlen;	/* aligned length of a key length */
    PageID 		leaf;		/* temporary PageID of a leaf page */
    PageID 		overflow;	/* temporary PageID of an overflow page */
    ObjectID 		*oidArray;	/* array of ObjectIDs */
    BtreeLeaf 		*apage;		/* pointer to a buffer holding a leaf page */
    BtreeOverflow 	*opage;		/* pointer to a buffer holding an overflow page */
    btm_LeafEntry 	*entry;		/* pointer to a leaf entry */    
    
    
    /* Error check whether using not supported functionality by EduBtM */
    int i;
    for(i=0; i<kdesc->nparts; i++)
    {
        if(kdesc->kpart[i].type!=SM_INT && kdesc->kpart[i].type!=SM_VARSTRING)
            ERR(eNOTSUPPORTED_EDUBTM);
    }

    /*
     * JBS Code
     */
    leaf = current->leaf;
    BfM_GetTrain(&leaf, (char **)&apage, PAGE_BUF);

    Boolean l_condSatisfied = FALSE;
    Two l_targetSlotNo = current->slotNo;
    Boolean l_isGetTrain = FALSE;
	KeyValue l_nowKval;

//	printf("comp : %d\n", compOp);
    switch (compOp) {
        case SM_EQ: // There is no duplicate key.
            break;
        case SM_GT:
        case SM_GE:
            l_targetSlotNo--;
            if (l_targetSlotNo < 0) {
                leaf.pageNo = apage->hdr.prevPage;
                leaf.volNo = apage->hdr.pid.volNo;
               	
				if (leaf.pageNo != NIL) {
					BfM_GetTrain(&leaf, (char **)&apage, PAGE_BUF);
					l_isGetTrain = TRUE;
					for (i=apage->hdr.nSlots-1; i>=0; --i) if (apage->slot[-i] != NIL) break;
					l_targetSlotNo = i;
				} else {
					break;
				}
            }
            entry = (btm_LeafEntry*)&apage->data[apage->slot[-l_targetSlotNo]];
			l_nowKval.len = entry->klen;
			memcpy(l_nowKval.val, entry->kval, entry->klen);
            cmp = edubtm_KeyCompare(kdesc, &l_nowKval, kval);

            if (compOp == SM_GT && cmp == GREATER) l_condSatisfied = TRUE;
            if (compOp == SM_GE && (cmp == GREATER || cmp == EQUAL)) l_condSatisfied = TRUE;

            if (l_isGetTrain == TRUE) BfM_FreeTrain(&leaf, PAGE_BUF);
            break;
        case SM_LT:
        case SM_LE:
            l_targetSlotNo++;
            if (l_targetSlotNo >= apage->hdr.nSlots) {
                leaf.pageNo = apage->hdr.nextPage;
                leaf.volNo = apage->hdr.pid.volNo;
               	 
				if (leaf.pageNo != NIL) {
					BfM_GetTrain(&leaf, (char **)&apage, PAGE_BUF);
					l_isGetTrain = TRUE;
					for (i=0; i<apage->hdr.nSlots; ++i) if (apage->slot[-i] != NIL) break;
					l_targetSlotNo = i;
				} else {
					break;
				}
            }
            entry = (btm_LeafEntry*)&apage->data[apage->slot[-l_targetSlotNo]];
			l_nowKval.len = entry->klen;
			memcpy(l_nowKval.val, entry->kval, entry->klen);
            cmp = edubtm_KeyCompare(kdesc, &l_nowKval, kval);

            if (compOp == SM_LT && cmp == LESS) l_condSatisfied = TRUE;
            if (compOp == SM_LE && (cmp == LESS || cmp == EQUAL)) l_condSatisfied = TRUE;
            
            if (l_isGetTrain == TRUE) BfM_FreeTrain(&leaf, PAGE_BUF);
            break;
        default: // Right?
            ERR(eNOTSUPPORTED_EDUBTM);
            break;
    }

    if (l_condSatisfied == TRUE) {
        next->flag = CURSOR_ON;
        alignedKlen = ALIGNED_LENGTH(entry->klen);
        memcpy(&next->oid, entry->kval+alignedKlen, sizeof(ObjectID));
        
        next->key.len = entry->klen;
        memcpy(next->key.val, entry->kval, entry->klen);
        next->leaf = leaf;
        next->slotNo = l_targetSlotNo;
    } else {
        next->flag = CURSOR_EOS;
    }
    
    BfM_FreeTrain(&current->leaf, PAGE_BUF);
    return(eNOERROR);
    
} /* edubtm_FetchNext() */
