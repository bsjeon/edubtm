/******************************************************************************/
/*                                                                            */
/*    ODYSSEUS/EduCOSMOS Educational-Purpose Object Storage System            */
/*                                                                            */
/*    Developed by Professor Kyu-Young Whang et al.                           */
/*                                                                            */
/*    Database and Multimedia Laboratory                                      */
/*                                                                            */
/*    Computer Science Department and                                         */
/*    Advanced Information Technology Research Center (AITrc)                 */
/*    Korea Advanced Institute of Science and Technology (KAIST)              */
/*                                                                            */
/*    e-mail: kywhang@cs.kaist.ac.kr                                          */
/*    phone: +82-42-350-7722                                                  */
/*    fax: +82-42-350-8380                                                    */
/*                                                                            */
/*    Copyright (c) 1995-2013 by Kyu-Young Whang                              */
/*                                                                            */
/*    All rights reserved. No part of this software may be reproduced,        */
/*    stored in a retrieval system, or transmitted, in any form or by any     */
/*    means, electronic, mechanical, photocopying, recording, or otherwise,   */
/*    without prior written permission of the copyright owner.                */
/*                                                                            */
/******************************************************************************/
/*
 * Module: EduBtM_Fetch.c
 *
 * Description :
 *  Find the first object satisfying the given condition.
 *  If there is no such object, then return with 'flag' field of cursor set
 *  to CURSOR_EOS. If there is an object satisfying the condition, then cursor
 *  points to the object position in the B+ tree and the object identifier
 *  is returned via 'cursor' parameter.
 *  The condition is given with a key value and a comparison operator;
 *  the comparison operator is one among SM_BOF, SM_EOF, SM_EQ, SM_LT, SM_LE, SM_GT, SM_GE.
 *
 * Exports:
 *  Four EduBtM_Fetch(PageID*, KeyDesc*, KeyValue*, Four, KeyValue*, Four, BtreeCursor*)
 */


#include <string.h>
#include "EduBtM_common.h"
#include "BfM.h"
#include "EduBtM_Internal.h"


/*@ Internal Function Prototypes */
Four edubtm_Fetch(PageID*, KeyDesc*, KeyValue*, Four, KeyValue*, Four, BtreeCursor*);



/*@================================
 * EduBtM_Fetch()
 *================================*/
/*
 * Function: Four EduBtM_Fetch(PageID*, KeyDesc*, KeyVlaue*, Four, KeyValue*, Four, BtreeCursor*)
 *
 * Description:
 * (Following description is for original ODYSSEUS/COSMOS BtM.
 *  For ODYSSEUS/EduCOSMOS EduBtM, refer to the EduBtM project manual.)
 *
 *  Find the first object satisfying the given condition. See above for detail.
 *
 * Returns:
 *  error code
 *    eBADPARAMETER_BTM
 *    some errors caused by function calls
 *
 * Side effects:
 *  cursor  : The found ObjectID and its position in the Btree Leaf
 *            (it may indicate a ObjectID in an  overflow page).
 */
Four EduBtM_Fetch(
    PageID   *root,		/* IN The current root of the subtree */
    KeyDesc  *kdesc,		/* IN Btree key descriptor */
    KeyValue *startKval,	/* IN key value of start condition */
    Four     startCompOp,	/* IN comparison operator of start condition */
    KeyValue *stopKval,		/* IN key value of stop condition */
    Four     stopCompOp,	/* IN comparison operator of stop condition */
    BtreeCursor *cursor)	/* OUT Btree Cursor */
{
    int i;
    Four e;		   /* error number */

    
    if (root == NULL) ERR(eBADPARAMETER_BTM);

    /* Error check whether using not supported functionality by EduBtM */
    for(i=0; i<kdesc->nparts; i++)
    {
        if(kdesc->kpart[i].type!=SM_INT && kdesc->kpart[i].type!=SM_VARSTRING)
            ERR(eNOTSUPPORTED_EDUBTM);
    }
    
    /*
     * JBS Code
     */
    if (startCompOp == SM_BOF) {
        edubtm_FirstObject(root, kdesc, stopKval, stopCompOp, cursor);
    } else if (startCompOp == SM_EOF){
        edubtm_LastObject(root, kdesc, stopKval, stopCompOp, cursor);
    } else {
        edubtm_Fetch(root, kdesc, startKval, startCompOp, stopKval, stopCompOp, cursor);
    }

    return(eNOERROR);

} /* EduBtM_Fetch() */



/*@================================
 * edubtm_Fetch()
 *================================*/
/*
 * Function: Four edubtm_Fetch(PageID*, KeyDesc*, KeyVlaue*, Four, KeyValue*, Four, BtreeCursor*)
 *
 * Description:
 * (Following description is for original ODYSSEUS/COSMOS BtM.
 *  For ODYSSEUS/EduCOSMOS EduBtM, refer to the EduBtM project manual.)
 *
 *  Find the first object satisfying the given condition.
 *  This function handles only the following conditions:
 *  SM_EQ, SM_LT, SM_LE, SM_GT, SM_GE.
 *
 * Returns:
 *  Error code *   
 *    eBADCOMPOP_BTM
 *    eBADBTREEPAGE_BTM
 *    some errors caused by function calls
 */
Four edubtm_Fetch(
    PageID              *root,          /* IN The current root of the subtree */
    KeyDesc             *kdesc,         /* IN Btree key descriptor */
    KeyValue            *startKval,     /* IN key value of start condition */
    Four                startCompOp,    /* IN comparison operator of start condition */
    KeyValue            *stopKval,      /* IN key value of stop condition */
    Four                stopCompOp,     /* IN comparison operator of stop condition */
    BtreeCursor         *cursor)        /* OUT Btree Cursor */
{
    Four                e;              /* error number */
    Four                cmp;            /* result of comparison */
    Two                 idx;            /* index */
    PageID              child;          /* child page when the root is an internla page */
    Two                 alignedKlen;    /* aligned size of the key length */
    BtreePage           *apage;         /* a Page Pointer to the given root */
    BtreeOverflow       *opage;         /* a page pointer if it necessary to access an overflow page */
    Boolean             found;          /* search result */
    PageID              *leafPid;       /* leaf page pointed by the cursor */
    Two                 slotNo;         /* slot pointed by the slot */
    PageID              ovPid;          /* PageID of the overflow page */
    PageNo              ovPageNo;       /* PageNo of the overflow page */
    PageID              prevPid;        /* PageID of the previous page */
    PageID              nextPid;        /* PageID of the next page */
    ObjectID            *oidArray;      /* array of the ObjectIDs */
    Two                 iEntryOffset;   /* starting offset of an internal entry */
    btm_InternalEntry   *iEntry;        /* an internal entry */
    Two                 lEntryOffset;   /* starting offset of a leaf entry */
    btm_LeafEntry       *lEntry;        /* a leaf entry */


    /* Error check whether using not supported functionality by EduBtM */
    int i;
    for(i=0; i<kdesc->nparts; i++)
    {
        if(kdesc->kpart[i].type!=SM_INT && kdesc->kpart[i].type!=SM_VARSTRING)
            ERR(eNOTSUPPORTED_EDUBTM);
    }

    /*
     * JBS Code
     */
    
    Boolean l_isStartCond = FALSE;
	Boolean l_isStopCond = FALSE;
    BfM_GetTrain(root, (char **)&apage, PAGE_BUF);
   
//	printf("Fetch Start!\n");

	if (apage->any.hdr.type & INTERNAL) {

		edubtm_BinarySearchInternal(&apage->bi, kdesc, startKval, &idx);

		iEntryOffset = apage->bi.slot[-idx];
		iEntry = (btm_InternalEntry*)&apage->bi.data[iEntryOffset];

		child.pageNo = iEntry->spid;
		child.volNo = apage->bi.hdr.pid.volNo;
	//	printf("idx, child:: %d, %d\n", idx, child.pageNo);
		edubtm_Fetch(&child, kdesc, startKval, startCompOp, stopKval, stopCompOp, cursor);
    } else {
		edubtm_BinarySearchLeaf(&apage->bl, kdesc, startKval, &idx);
		
		lEntryOffset = apage->bl.slot[-idx];
        lEntry = (btm_LeafEntry*)&apage->bl.data[lEntryOffset];
 
		KeyValue l_nowKval;
        l_nowKval.len = lEntry->klen;
        memcpy(l_nowKval.val, lEntry->kval, lEntry->klen);
        cmp = edubtm_KeyCompare(kdesc, &l_nowKval, startKval);
  
		// Start Option Validity check
		if (startCompOp == SM_EQ) {
			if (cmp == EQUAL) l_isStartCond = TRUE;
		} else if (startCompOp == SM_LT || startCompOp == SM_LE) {
			// Greater case is always FALSE
			if (cmp == GREATER) l_isStartCond = FALSE;
			else if (cmp == LESS) l_isStartCond = TRUE;
			else if (startCompOp == SM_LE && cmp == EQUAL) l_isStartCond = TRUE;
			else if (startCompOp == SM_LT && cmp == EQUAL) {
				int l_preIdx = idx;
				while (idx-1 >= 0) {
					--idx;
					if (apage->bl.slot[-idx]!= NIL) break;
				}
				if (idx < l_preIdx && apage->bl.slot[-idx]!=NIL) l_isStartCond = TRUE;
				else {
					BtreePage *l_prevPage;	
					PageID l_prevPid;
					if (apage->bl.hdr.prevPage != NIL) {
						l_prevPid.pageNo = apage->bl.hdr.prevPage;
						l_prevPid.volNo = apage->bl.hdr.pid.volNo;
						edubtm_Fetch(&l_prevPid, kdesc, startKval, startCompOp, stopKval, stopCompOp, cursor);
						BfM_FreeTrain(root, PAGE_BUF);
						return eNOERROR;
					} 
				}
			}
		} else if (startCompOp == SM_GT || startCompOp == SM_GE) {
			if (cmp == GREATER) l_isStartCond = TRUE; // This is impossible
			else if (cmp == EQUAL && startCompOp == SM_GE) l_isStartCond = TRUE;
			else {
				int l_preIdx = idx;
				while (idx+1 < apage->bl.hdr.nSlots) {
					++idx;
					if (apage->bl.slot[-idx]!= NIL) break;
				}
				if (idx > l_preIdx && apage->bl.slot[-idx] != NIL) l_isStartCond = TRUE;
				else {
					BtreePage *l_nextPage;	
					PageID l_nextPid;
					if (apage->bl.hdr.nextPage != NIL) {
						l_nextPid.pageNo = apage->bl.hdr.nextPage;
						l_nextPid.volNo = apage->bl.hdr.pid.volNo;
						edubtm_Fetch(&l_nextPid, kdesc, startKval, startCompOp, stopKval, stopCompOp, cursor);
						BfM_FreeTrain(root, PAGE_BUF);
						return eNOERROR;
					} 	
				}
			}
		}

	//	printf("Compare :%d %d %d %d \n",idx, *(int*)l_nowKval.val, *(int*)stopKval->val, cmp);
       
		if (l_isStartCond == TRUE) {
			lEntryOffset = apage->bl.slot[-idx];
			lEntry = (btm_LeafEntry*)&apage->bl.data[lEntryOffset];

			l_nowKval.len = lEntry->klen;
			memcpy(l_nowKval.val, lEntry->kval, lEntry->klen);
			cmp = edubtm_KeyCompare(kdesc, &l_nowKval, stopKval);

			switch (stopCompOp) {
				case SM_EQ:
					if (cmp == EQUAL) l_isStopCond = TRUE;
					break;
				case SM_LT:
					if (cmp == LESS) l_isStopCond = TRUE;
					break;
				case SM_LE:
					if (cmp == LESS || cmp == EQUAL) l_isStopCond = TRUE;
					break;
				case SM_GT:
					if (cmp == GREATER) l_isStopCond = TRUE;
					break;
				case SM_GE:
					if (cmp == GREATER || cmp == EQUAL) l_isStopCond = TRUE;
					break;
				default: // Right?
					ERR(eNOTSUPPORTED_EDUBTM);
					break;
			}
			if (l_isStopCond == FALSE) {
				cursor->flag = CURSOR_EOS;	
			} else {
				cursor->flag = CURSOR_ON;
				alignedKlen = ALIGNED_LENGTH(lEntry->klen);
				memcpy(&cursor->oid, lEntry->kval+alignedKlen, sizeof(ObjectID));

				cursor->key = l_nowKval;
				cursor->leaf = apage->bl.hdr.pid;
				cursor->slotNo = idx;
			}
		} else cursor->flag = CURSOR_EOS;
	}
 
    BfM_FreeTrain(root, PAGE_BUF);
    
    // If no value, then return Error ? or not?
    return(eNOERROR);
    
} /* edubtm_Fetch() */

