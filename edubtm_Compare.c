/******************************************************************************/
/*                                                                            */
/*    ODYSSEUS/EduCOSMOS Educational-Purpose Object Storage System            */
/*                                                                            */
/*    Developed by Professor Kyu-Young Whang et al.                           */
/*                                                                            */
/*    Database and Multimedia Laboratory                                      */
/*                                                                            */
/*    Computer Science Department and                                         */
/*    Advanced Information Technology Research Center (AITrc)                 */
/*    Korea Advanced Institute of Science and Technology (KAIST)              */
/*                                                                            */
/*    e-mail: kywhang@cs.kaist.ac.kr                                          */
/*    phone: +82-42-350-7722                                                  */
/*    fax: +82-42-350-8380                                                    */
/*                                                                            */
/*    Copyright (c) 1995-2013 by Kyu-Young Whang                              */
/*                                                                            */
/*    All rights reserved. No part of this software may be reproduced,        */
/*    stored in a retrieval system, or transmitted, in any form or by any     */
/*    means, electronic, mechanical, photocopying, recording, or otherwise,   */
/*    without prior written permission of the copyright owner.                */
/*                                                                            */
/******************************************************************************/
/*
 * Module: edubtm_Compare.c
 *
 * Description : 
 *  This file includes two compare routines, one for keys used in Btree Index
 *  and another for ObjectIDs.
 *
 * Exports: 
 *  Four edubtm_KeyCompare(KeyDesc*, KeyValue*, KeyValue*)
 *  Four edubtm_ObjectIdComp(ObjectID*, ObjectID*)
 */


#include <string.h>
#include "EduBtM_common.h"
#include "EduBtM_Internal.h"



/*@================================
 * edubtm_KeyCompare()
 *================================*/
/*
 * Function: Four edubtm_KeyCompare(KeyDesc*, KeyValue*, KeyValue*)
 *
 * Description:
 * (Following description is for original ODYSSEUS/COSMOS BtM.
 *  For ODYSSEUS/EduCOSMOS EduBtM, refer to the EduBtM project manual.)
 *
 *  Compare key1 with key2.
 *  key1 and key2 are described by the given parameter "kdesc".
 *
 * Returns:
 *  result of omparison (positive numbers)
 *    EQUAL : key1 and key2 are same
 *    GREAT : key1 is greater than key2
 *    LESS  : key1 is less than key2
 *
 * Note:
 *  We assume that the input data are all valid.
 *  User should check the KeyDesc is valid.
 */
Four edubtm_KeyCompare(
    KeyDesc                     *kdesc,		/* IN key descriptor for key1 and key2 */
    KeyValue                    *key1,		/* IN the first key value */
    KeyValue                    *key2)		/* IN the second key value */
{
    register unsigned char      *left;          /* left key value */
    register unsigned char      *right;         /* right key value */
    Two                         i;              /* index for # of key parts */
    Two                         j;              /* temporary variable */
    Two                         kpartSize;      /* size of the current kpart */
    Two                         len1, len2;	/* string length */
    Two_Invariable              s1, s2;         /* 2-byte short values */
    Four_Invariable             i1, i2;         /* 4-byte int values */
    Four_Invariable             l1, l2;         /* 4-byte long values */
    Eight_Invariable            ll1, ll2;       /* 8-byte long long values */
    float                       f1, f2;         /* float values */
    double                      d1, d2;		/* double values */
    PageID                      pid1, pid2;	/* PageID values */
    OID                         oid1, oid2;     /* OID values */
    

    /* Error check whether using not supported functionality by EduBtM */
    for(i=0; i<kdesc->nparts; i++)
    {
        if(kdesc->kpart[i].type!=SM_INT && kdesc->kpart[i].type!=SM_VARSTRING)
            ERR(eNOTSUPPORTED_EDUBTM);
    }

    /*
     * JBS Code
     */
    left = key1->val;
    right = key2->val;

    for (i=0; i<kdesc->nparts; ++i) {
        if (kdesc->kpart[i].type == SM_INT) {
            switch (kdesc->kpart[i].length) {
                case SM_SHORT_SIZE:
                    s1 = *((Two_Invariable*)left);
                    s2 = *((Two_Invariable*)right);
                    if (s1>s2) return GREATER;
                    else if (s1 < s2) return LESS;
                    break;
                case SM_INT_SIZE:
                    i1 = *((Four_Invariable*)left);
                    i2 = *((Four_Invariable*)right);
                    if (i1>i2) return GREATER;
                    else if (i1 < i2) return LESS;
                    break;
                case SM_LONG_LONG_SIZE:
                    ll1 = *((Eight_Invariable*)left);
                    ll2 = *((Eight_Invariable*)right);
                    if (ll1>ll2) return GREATER;
                    else if (ll1 < ll2) return LESS;
                    break;
            }

            left += kdesc->kpart[i].length;
            right += kdesc->kpart[i].length;
        } else {
            // SM_VARSTRING CASE
            len1 = *((Two_Invariable*)left);
            len2 = *((Two_Invariable*)right);
            left += sizeof(Two);
            right+=sizeof(Two);
            
            // Temporary string
            char *ltmp, *rtmp;
            ltmp = (char*)malloc(len1);
            rtmp = (char*)malloc(len2);
            memcpy(ltmp, left, len1);
            memcpy(rtmp, right, len2);
            j = strcmp(ltmp, rtmp);

            if (j>0) return GREATER;
            else return LESS;
            
            left += len1;
            right += len2;
        }
    }
    
    return(EQUAL);
    
}   /* edubtm_KeyCompare() */
